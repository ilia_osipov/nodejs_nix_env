lint:
  yarn eslint .

format:
  yarn prettier . --write

build:
  echo "Nothing to build"

run:
  node ./src/app.js '[2, 3, 4, 5]'

test:
  NODE_OPTIONS=--experimental-vm-modules yarn jest

production: build format lint test
  pm2 start app.js --name app  

stop:
  pm2 stop nix-shell-env
  pm2 delete 0
  pm2 ls
